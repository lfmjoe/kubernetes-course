var axios = require('axios');
var express = require('express');
var router = express.Router();

/* GET home page. */
router.get('/', function(req, res, next) {
  axios.get('http://restapi.learnk8s.org/world')
  .then(response => {
    res.render('index', { title: response.value });
  })
  .catch(error => {
    console.log(error);
    res.render('index', { title: 'Error' });
  });
});

module.exports = router;
