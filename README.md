# Full Custer Deployment Walkthrough
This guide walks through a basic AWS Deployment on EKS with a couple of Node.js applications and a few common services

## Create Cluster
The first task is to set up the EKS cluster.  Using `eksctl` we are able to provide the EKS cluster inside of a VPC with the appropriate subnets, load balancer, and other base deployment services and features configured.  This step will create two CloudFormation templates - one for the cluster and another for the worker nodes.
```eksctl create cluster -f cluster.yaml```

Once the cluster is ready and available we can begin to configure the applications and other services.

## Create Namespace
For this demo we will create a new namespace to put everything in.  This will help isolate the resources and provide a boundary inside of the Kubernetes cluster so it could potentially hold multiple environments.
```kubectl apply -f saisoft-namespace.yaml```

## Install NGINX Ingress
We'll begin by deploying the ingress controller.  For this we will use Nginx Helm Chart.  The Helm deploy will deploy all required components and configurations.  Updates can be made using a `values.yaml` and adding `-f values.yaml` to the following command.

```helm install nginx-stable/nginx-ingress --generate-name -n saisoft```

This will deploy the ingress to the default namespace.  If you'd like to use another namespace such as `kube-system` you can add `-n kube-system` to deploy the helm chart to another namespace.

## Add Metrics Server
Next we'll deploy the metrics server.  This is a local copy of `kubectl apply -f https://github.com/kubernetes-sigs/metrics-server/releases/download/v0.3.6/components.yaml`
```kubectl apply -f metrics-deployment.yaml```

## Install Kubernetes Dashboard
With the metrics server running we can now deploy the Kubernetes Dashboard.  This will allow us to view metrics and information about our Kubernetes Cluster.
```kubectl apply -f dashboard-deployment.yaml```

## Internal Dashboard Access
The best pratice to access Kubernetes Dashboard is privately through the kubectl proxy.  The steps below detail the procedure.

### Get AWS Token
We first need to retrieve the tokens.
```kubectl -n kube-system get secret```

Find the token that beings with `eks-admin` and run the following command replacing the eks-admin-token-xxx with the name you found in your list of secrets.
```kubectl -n kube-system describe secret eks-admin-token-rrnmn```

The output should look similar to this:
```eyJhbGciOiJSUzI1NiIsImtpZCI6IiJ9.eyJpc3MiOiJrdWJlcm5ldGVzL3NlcnZpY2VhY2NvdW50Iiwia3ViZXJuZXRlcy5pby9zZXJ2aWNlYWNjb3VudC9uYW1lc3BhY2UiOiJrdWJlLXN5c3RlbSIsImt1YmVybmV0ZXMuaW8vc2VydmljZWFjY291bnQvc2VjcmV0Lm5hbWUiOiJla3MtYWRtaW4tdG9rZW4tcnJubW4iLCJrdWJlcm5ldGVzLmlvL3NlcnZpY2VhY2NvdW50L3NlcnZpY2UtYWNjb3VudC5uYW1lIjoiZWtzLWFkbWluIiwia3ViZXJuZXRlcy5pby9zZXJ2aWNlYWNjb3VudC9zZXJ2aWNlLWFjY291bnQudWlkIjoiNTdmMzVmODMtYWM2Mi00NjgyLWI2NjMtNGQ0OTZmMTE4MWEyIiwic3ViIjoic3lzdGVtOnNlcnZpY2VhY2NvdW50Omt1YmUtc3lzdGVtOmVrcy1hZG1pbiJ9.avQeZxvmVUaTC37labRg0HBFE0lyjFm41AXDFD3ihdTd35ls1PaqrhfuYNiC-oASroYxBeHcYV60s2UiSNOm3MFyIE1hf-BvPTIiWcnpXNbD0YGv-2Nju2e8rYSVZH4l5s49jg5WwPZlt2AQFtbTPhQo3KhhHKGrcj17Wf4rMowbBj6288YF7bDUgzHLzGrCV-rYVhAdDgRC33PG9P5LhdQnFMkqiigaCEez1AZ6v_DUHaHn7lMGRKqA2Ik4AKcSF18pOB-XhtuTqy4UXNnDZ8dIbNyAfaJJJFNhZXa3AxIq0pT6Eygq8DkwhHJMZDvc_cntFA2xsKeZFoclzEEgZQ```

### Connect through proxy
Once you have the token connect using the proxy.
```kubectl proxy```

You can then browse to `http://localhost:8001/api/v1/namespaces/kubernetes-dashboard/services/https:kubernetes-dashboard:/proxy/#/` and login using the token you retrieved in the prior steps.  This will validate that Kubernetes Dashboard has been installed properly.

## Connect to AWS ECR
EKS will need to pull images so we will set up a repository in AWS using ECR.  To enable it to work locally we need to have docker login to the repository so we can push images we build locally to the remote repository.
```aws ecr get-login-password --region us-east-1 | docker login --username AWS --password-stdin <aws-account>.dkr.ecr.<region>.amazonaws.com```

## Application Installations
Included are two applications that will be installed on the cluster.  Both are simple Node.js applications.  One application provides a simple API that is consumed by the second application.

### Rest API Application
The `restapi` application is a simple api that produces a json `{value: 'World'}`.

To use this image we will first need to build it.  Note the url and / before the restapi:1.0.0.  This is used so when we do the second push the image is sent to the remote repository. 
```docker build -t <aws-account>.dkr.ecr.<region>.amazonaws.com/restapi:1.0.0 .```

After building the docker image we can go ahead and push it to the remote.  If your login has timed out you will need to run the above docker login command again.
```docker push <aws-account>.dkr.ecr.<region>.amazonaws.com/restapi:1.0.0```

Once the docker is pushed you can navigate to the `restapi/chart` folder.  From here you can run the following command to install the Helm Chart to the cluster. 
```helm install restapi . -n saisoft```

Once the Helm Chart has been installed you can verify the installation was successful in the Kubernetes Dashboard.

We can also verify the API by browsing to `http://restapi.learnk8s.org/world`

### Node Frontend Application
Same as with `restapi` we will now build the `nodeapp`.
```docker build -t <aws-account>.dkr.ecr.<region>.amazonaws.com/nodeapp:1.0.0 .```

Same push as above
```docker push <aws-account>.dkr.ecr.<region>.amazonaws.com/nodeapp:1.0.0```

This application is missing the Helm Chart files.  So we can go ahead and create it with the following command.
```helm chart create```

After the helm chart files are created we need to update `service.yaml` and `deployment.yaml`.  Compare and copy the values from the `restapi` Helm Chart.

Once the Helm Chart has been update we can deploy it with the below command.
```helm install nodeapp . -n saisoft```

Once the Helm Chart has been deployed and verified in the Kubernetes Dashboard we can browse to `http://nodeapp.learnk8s.org` to see it in action.
